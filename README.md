# Lab 2

Docker Compose project to run a web application from multiple repositories with a LetsEncrypt certificate.

## Run

Build the backend and frontend images. In production you would push them to a remote registry, but for testing the local registry is enough.

```shell
git clone https://gitlab.com/jvstme-devops-labs/lab-6-backend.git
cd lab-6-backend
docker build -t lab-6-backend .
cd ..

git clone https://gitlab.com/jvstme-devops-labs/lab-6-frontend.git
cd lab-6-frontend
docker build -t lab-6-frontend .
cd ..
```

Clone the Deploy repo and set your domain name and email in `.env`.

```shell
git clone https://gitlab.com/jvstme-devops-labs/lab-6-deploy.git
cd lab-6-deploy
$EDITOR .env
```

Run the project.

```shell
docker compose up
```
